import React from "react";
import Table from "./Table.js";
import Footer from "./Footer.js";
import { useTranslation } from "react-i18next";

const Panneau = () => {
  const { t } = useTranslation();

  return (
    <div className="border">
      <h1 className="">{t("depart")}</h1>
      <Table />
      <Footer />
    </div>
  );
};

export default Panneau;
