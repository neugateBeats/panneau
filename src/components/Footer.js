import React from 'react';

const Footer = () => {
    return(
        <footer>
            <div>Aeroport International d'Andorra - Chambre de commerce et d'industrie d'Andore</div>
        </footer>
    )
}

export default Footer;