import React from "react";
import Vol from "./Vol.js";
import { useEffect, useState } from "react";

const Table = () => {
  const [vols, setVols] = useState([]);
  useEffect(() => {
    const timerId = setInterval(() => {
      fetch("http://localhost:8080/api/v1/vols")
        .then((response) => response.json())
        .then((data) => setVols(data));
    }, 1000);
    //La fonction de callback du useEffect doit renvoyer une autre callback qui clear ce timer
    return () => clearInterval(timerId);
  });

  return (
    <table>
      <thead>
        <tr>
          <th>Heure</th>
          <th>Destination</th>
          <th>Vol</th>
          <th>Porte</th>
          <th>Remarque</th>
        </tr>
      </thead>
      <tbody>
        {vols.map((vol) => (
          <Vol vol={vol} key={vol.id} />
        ))}
      </tbody>
      <tfoot>
        <tr>
          <td>Mis à jours à 14h01</td>
        </tr>
      </tfoot>
    </table>
  );
};

export default Table;
