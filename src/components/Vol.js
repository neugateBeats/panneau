import React from "react";

const Vol = (props) => {
    return(
        <tr className={props.vol.class}>
            <td>{props.vol.heure}</td>
            <td>{props.vol.destination}</td>
            <td>{props.vol.vol}</td>
            <td>{props.vol.porte}</td>
            <td>{props.vol.remarque}</td>
        </tr>
    )
}

export default Vol;
