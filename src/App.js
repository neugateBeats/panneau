// import React from "react";
// import i18n from "./i18n";
// import { I18nextProvider } from "react-i18next";
import Panneau from "./components/Panneau";
import React, { useState } from "react";
import i18next from "i18next";
import resources from "./i18n.js";

const App = () => {
  const [language, setLanguage] = useState("fr");
  i18next.init({
    resources,
    lng: language,
    fallbackLng: "en",
  });
  const handlelanguageChange = (lng) => {
    setLanguage(lng);
    i18next.changeLanguage(lng);
  };
  console.log(handlelanguageChange);
  return (
    <div>
      <button onClick={() => handlelanguageChange("fr")}>Français</button>
      <button onClick={() => handlelanguageChange("en")}>Anglais</button>
      <button onClick={() => handlelanguageChange("es")}>Espagnol</button>
      <Panneau />
    </div>
    //  <I18nextProvider i18n={i18n}>
    //    <Panneau />
    //  </I18nextProvider>
  );
};

export default App;
