// Importation du module i18n de i18next
import i18n from "i18next";
// Importation de la fonction initReactI18next pour initialiser i18next avec React
import { initReactI18next } from "react-i18next";

i18n.use(initReactI18next).init({
  // Définition des resources pour les différentes langues supportées (français, anglais et espagnol)
  resources: {
    fr: {
      translation: {
        depart: "Départ",
      },
    },
    en: {
      translation: {
        depart: "Departure",
      },
    },
    es: {
      translation: {
        depart: "Salida",
      },
    },
  },
  // Définition de la langue par défaut (français)
  lng: "fr",
  // Définition de la langue de secours (anglais)
  fallbackLng: "en",
  // Définition des options d'interpolation
  interpolation: {
    // Désactivation de l'échappement des valeurs
    escapeValue: false,
  },
});

export default i18n;
