import express from 'express';
const app = express();

const infos = {
    
    data: [
        {
            id: '1',
            heure: '13:00:00',
            destination: 'Paris',
            vol: 'AF 222',
            porte: 'A12',
            remarque: 'Parti',
            class: 'vol-ok'
        },
        {
            id: '2',
            heure: '14:30:00',
            destination: 'Turin',
            vol: 'MP 282',
            porte: 'A13',
            remarque: 'Ponctuel',
            class: 'vol-ok'
        },
        {
            id: '3',
            heure: '16:00:00',
            destination: 'Montpellier',
            vol: 'DF 587',
            porte: 'A15',
            remarque: 'Annule',
            class: 'vol-annule'
        },
        {
            id: '4',
            heure: '16:00:00',
            destination: 'Martinique',
            vol: 'DF 587',
            porte: 'A15',
            remarque: 'Annule',
            class: 'vol-annule'
        }
    ]
}

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', '*');
    next();
})

app.get('/api/v1/vols', (req, res) => {
    res.json(infos.data);
})

app.listen(8080, () => {
    console.log('Server started on port 8080');
})